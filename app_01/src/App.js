import React from 'react';
import './App.css';


function App() {

  const [open,setOpen] = React.useState(false)
  const [open2,setOpen2] = React.useState(false)
  return (
    <div className="App">
       <button onClick={()=>setOpen(true)} className="open-modal-btn"> Open first modal</button>
       <button onClick={()=>setOpen2(true)} className="open-modal-btn"> Open second modal</button>
     {
      open && (
          <div className="overlay" >
        <div className="modal">
          <svg onClick={()=>setOpen(false)} height="200" viewBox="0 0 200 200" width="200">
            <title />
            <path d="M114,100l49-49a9.9,9.9,0,0,0-14-14L100,86,51,37A9.9,9.9,0,0,0,37,51l49,49L37,149a9.9,9.9,0,0,0,14,14l49-49,49,49a9.9,9.9,0,0,0,14-14Z" />
          </svg>
         <h2>Hello!</h2>
         <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quasi, accusamus!</p>
        </div>
      </div> 
      )
     }
     {
      open2 && (
          <div className="overlay_modal_two" >
        <div className="modal_two">
          <svg onClick={()=>setOpen2(false)} height="200" viewBox="0 0 200 200" width="200">
            <title />
            <path d="M114,100l49-49a9.9,9.9,0,0,0-14-14L100,86,51,37A9.9,9.9,0,0,0,37,51l49,49L37,149a9.9,9.9,0,0,0,14,14l49-49,49,49a9.9,9.9,0,0,0,14-14Z" />
          </svg>
         <h2 className='text_modal'>Hello!</h2>
         <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum consectetur assumenda laboriosam ullam accusamus temporibus? Illum, optio asperiores vitae recusandae sit praesentium alias accusamus magnam, distinctio ad fugit facilis illo.</p>
        </div>
      </div> 
      )
     }
    </div>
  );
}

export default App;
